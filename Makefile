#!/usr/bin/make -f
SHELL=/bin/bash
.ONESHELL:
.PHONY: run all clean test zip


HEADER_FILES=$(shell find -name "*.h")
CPP_FILES=$(shell find -name "*.cpp")
BIN=crazyrobot

ZIP_FILES = $(shell find -name "*.cpp" -or -name "*.h") Makefile
ZIP_PACKAGE_NAME = LucasCaetanoPossatti.zip


all: $(BIN)

run: all
	./$(BIN)

clean:
	rm -f "$(BIN)" "$(ZIP_PACKAGE_NAME)" "$(TEST_BIN)"

zip: $(ZIP_PACKAGE_NAME)


$(BIN): $(HEADER_FILES) $(CPP_FILES)
	g++ $(^) -o $(@) -lGL -lGLU -lglut -lm -std=c++11

$(ZIP_PACKAGE_NAME): clean $(ZIP_FILES)
	zip -r $(@) $(?)
