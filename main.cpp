#include <GL/glut.h>
#include <iostream>
#include <cmath>

using namespace std;

#define paddleHeight 80
#define paddleWidth 10
#define baseHeight 40
#define baseWidth 100
#define radiusWheel 30

struct RGB {
	float red, green, blue;
};

RGB BLACK = {0,0,0};
RGB WHITE = {1,1,1};

void drawRectangle(float width, float height, RGB color=WHITE) {
	glColor3d(color.red, color.green, color.blue);
	glBegin(GL_QUADS);
        glVertex2f(-width/2, 0);
        glVertex2f(-width/2, height);
        glVertex2f(width/2, height);
        glVertex2f(width/2, 0);
    glEnd();
}

void drawCircle(/*float cx, float cy,*/ float radius, RGB color=WHITE, int segments=32) {
	float cx=0, cy=0;
	glColor3d(color.red, color.green, color.blue);
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(cx, cy);
	for(int i = 0; i <= segments;i++) {
		glVertex2f(
			cx + (radius * cos(i * 2*M_PI / segments)),
			cy + (radius * sin(i * 2*M_PI / segments))
		);
	}
	glEnd();
}

void drawHollowCircle(/*float cx, float cy,*/ float radius, RGB color=WHITE, int segments=32) {
	float cx=0, cy=0;
	glColor3d(color.red, color.green, color.blue);
	glPointSize(3);
	glBegin(GL_POINTS);
	glVertex2f(cx, cy);
	for(int i = 0; i < segments; i++) {
		glVertex2f(
			cx + (radius * cos(i * 2*M_PI / segments)),
			cy + (radius * sin(i * 2*M_PI / segments))
		);
	}
	glEnd();
}

class CrazyRobot {
public:
	GLfloat x = 0, y = 0;
	GLfloat theta1 = 0;
	GLfloat theta2 = 0;
	GLfloat theta3 = 0;
	GLfloat thetaWheel = 0;
	GLfloat direction = -1;

	void draw() {
		// cout << "Robot: x=" << this->x << ", y=" << this->y << endl;
		glPushMatrix();
		glTranslatef(this->x, this->y, 0);
		drawRectangle(baseWidth, baseHeight, {1,0,0});

		// Draw arms.
		glPushMatrix();
		glTranslatef(0, baseHeight, 0);
		glRotatef(this->theta1, 0, 0, 1);
		drawRectangle(paddleWidth, paddleHeight, {0,1,0});
		glTranslatef(0, paddleHeight, 0);
		glRotatef(this->theta2, 0, 0, 1);
		drawRectangle(paddleWidth, paddleHeight, {0,0,1});
		glTranslatef(0, paddleHeight, 0);
		glRotatef(this->theta3, 0, 0, 1);
		drawRectangle(paddleWidth, paddleHeight, {1,1,0});
		glPopMatrix();

		// Draw wheels.
		glPushMatrix();
		glTranslatef(baseWidth/2, 0, 0);
		glRotatef(this->thetaWheel, 0, 0, 1);
		drawHollowCircle(radiusWheel, {1,1,1}, 10);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(-baseWidth/2, 0, 0);
		glRotatef(this->thetaWheel, 0, 0, 1);
		drawHollowCircle(radiusWheel, {1,1,1}, 10);
		glPopMatrix();

		glPopMatrix();
	}
};

CrazyRobot robot;
int windowWidth = 700, windowHeight = 700;
float maxSpeed = 5;
float maxAngularSpeed = 0.3;
float speedX = 0, speedY = 0;
float speedTheta1 = 0, speedTheta2 = 0, speedTheta3 = 0;


void display() {
	glClear(GL_COLOR_BUFFER_BIT);

	robot.draw();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	cout << "Keyboard: " << key << endl;
	if (key == 'w') {
		speedY = maxSpeed;
	} else if (key == 's') {
		speedY = -maxSpeed;
	}
	if (key == 'd') {
		speedX = maxSpeed;
	} else if (key == 'a') {
		speedX = -maxSpeed;
	}
	if (key == 'r') {
		speedTheta1 = maxAngularSpeed;
	} else if (key == 'f') {
		speedTheta1 = -maxAngularSpeed;
	}
	if (key == 't') {
		speedTheta2 = maxAngularSpeed;
	} else if (key == 'g') {
		speedTheta2 = -maxAngularSpeed;
	}
	if (key == 'y') {
		speedTheta3 = maxAngularSpeed;
	} else if (key == 'h') {
		speedTheta3 = -maxAngularSpeed;
	}
}

void keyboardup(unsigned char key, int x, int y) {
	cout << "Keyboard Up: " << key << endl;
	if (key == 'w' || key == 's') {
		speedY = 0;
	}
	if (key == 'd' || key == 'a') {
		speedX = 0;
	}
	if (key == 'r' || key == 'f') {
		speedTheta1 = 0;
	}
	if (key == 't' || key == 'g') {
		speedTheta2 = 0;
	}
	if (key == 'y' || key == 'h') {
		speedTheta3 = 0;
	}
}

void idle() {
	robot.x += speedX;
	robot.y += speedY;
	robot.theta1 += speedTheta1;
	robot.theta2 += speedTheta2;
	robot.theta3 += speedTheta3;
	robot.thetaWheel -= speedX*360/(2*M_PI*radiusWheel);
	glutPostRedisplay();
}

void init() {
	glClearColor(0,0,0,1);
	glMatrixMode(GL_PROJECTION);
	glOrtho(
		-windowWidth/2,
		windowWidth/2,
		-windowHeight/2,
		windowHeight/2,
		100,
		-100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Crazy Robot");
	init();
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardup);
	glutMainLoop();

	return 0;
}
